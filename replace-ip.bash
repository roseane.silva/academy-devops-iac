#!/bin/bash
ip=$(<app_external_ip.txt)
echo "$ip"

hosts_file="ansible/hosts"
echo "$hosts_file"

if [ "$ip" != "" ]; then
  sed -i "s/[0-9]\{2\}\.[0-9]\{2\}\.[0-9]\{3\}\.[0-9]\{2\}/$ip/" $hosts_file
fi