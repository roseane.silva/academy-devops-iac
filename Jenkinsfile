pipeline {
    agent any

    environment {
        ARTIFACTORY = credentials('artifactory')
    }

    stages {
        stage ('Checkout') {
            steps {
                checkout([$class: 'GitSCM', branches: [[name: '*/main']], userRemoteConfigs: [[url: 'https://gitlab.com/roseane.silva/academy-devops-app.git']]])
            }
        }

        stage ('Build') {
            agent {
                docker {
                    image 'openjdk:11'
                    args '-v "$PWD":/app'
                    reuseNode true
                }
            }
            steps {
                sh './gradlew clean build'
            }
        }

        stage ('Build docker image') {
            steps {

                sh 'docker build -t roseanesilva/people-devops-academy:1.0 .'

            }
        }

        stage ('Push docker image') {
            steps {
                withDockerRegistry(credentialsId: 'dockerhub', url: 'https://index.docker.io/v1/') {
                    sh 'docker push roseanesilva/people-devops-academy:1.0'
                }
            }
        }

        stage ('Publish to Artifactory') {
            steps {
                sh './gradlew publish --no-daemon'
            }
        }

        stage ('Checkout IaC') {
            steps {
                checkout([$class: 'GitSCM', branches: [[name: '*/main']], userRemoteConfigs: [[url: 'https://gitlab.com/roseane.silva/academy-devops-iac.git']]])
            }
        }

        stage ("Terraform apply") {
            steps {
                dir("terraform") {
                    sh "terraform init"
                    sh "terraform apply --auto-approve -lock=false"
                }
          }
        }


        stage ("Configure App IP address") {
            steps {
                //add execute permission
                sh "chmod +x /var/jenkins_home/workspace/ac-academy/replace-ip.bash"
                sh "/var/jenkins_home/workspace/ac-academy/replace-ip.bash"
            }
        }

        stage ("Run Config w/ Ansible") {
            steps {
                dir("ansible") {
                    echo "Configuring App instance by running Ansible"
                    sh "ansible-playbook -i hosts playbook_artifactory.yaml"
                }
            }
        }

    }
}