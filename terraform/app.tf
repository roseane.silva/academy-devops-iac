terraform {
  backend "gcs" {
    bucket = "academy-terraform-state"
    prefix = "root/network-tfstate"
    credentials = "/usr/local/ac-academy-02-ea5ffb7d3db5.json"
  }
}

provider "google" {
  project = var.project_id
  region  = var.region
  zone    = var.zone
  credentials = var.credential_location
}

resource "google_service_account" "default" {
  account_id   = "academy-rs"
  display_name = "Service Account"
}

resource "google_compute_instance_iam_member" "member" {
  project = var.project_id
  zone = var.zone
  instance_name = google_compute_instance.app_vm.name
  role = "roles/compute.admin"
  member = "serviceAccount:${google_service_account.default.email}"
}

resource "google_compute_instance" "app_vm" {
  name = var.app_name
  machine_type = var.machine_type
  tags = ["allow-inbound"]

  service_account {
    email  = google_service_account.default.email
    scopes = ["cloud-platform"]
  }

  boot_disk {
    device_name = var.app_name
    initialize_params {
      image = "debian-10-buster-v20210817"
      size = 10
      type = "pd-balanced"

    }
  }
  network_interface {
    network = "default"
    access_config {

    }
  }
}

resource "google_compute_firewall" "rules" {
  project = var.project_id
  name = "allow-inbound"
  network = "default"
  direction = "INGRESS"
  priority = 1000
  source_ranges = ["0.0.0.0/0"]
  allow {
    protocol = "tcp"
  }
  target_tags = ["allow-inbound"]
}

output "app_external_ip" {
  description = "The external IP address of the deployed App instance"
  value       = google_compute_instance.app_vm.network_interface.0.access_config.0.nat_ip
}

resource "local_file" "external_ip" {
  content  = google_compute_instance.app_vm.network_interface.0.access_config.0.nat_ip
  filename = "../app_external_ip.txt"
}