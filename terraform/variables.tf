variable "docker_image" {
  description = "The docker image to be used"
  default = "docker.io/roseanesilva/mongo-devops-academy:latest"
}

variable "project_id" {
  default = "ac-academy-02"
}

variable "region" {
  default = "us-central1"
}

variable "zone" {
  default = "us-central1-a"
}

variable "machine_type" {
  default = "e2-micro"
}

variable "app_name" {
  default = "my-cloud-app"
}

variable "db_name" {
  default = "my-cloud-db"
}

variable "credential_location" {
  default = "/usr/local/ac-academy-02-ea5ffb7d3db5.json"
}