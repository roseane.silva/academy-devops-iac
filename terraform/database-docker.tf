#module "gce-container" {
#  source = "terraform-google-modules/container-vm/google"
#
#  container = {
#    image = var.docker_image
#  }
#  restart_policy = "Always"
#}
#
#resource "google_compute_instance" "db_vm" {
#  name = var.db_name
#  machine_type = var.machine_type
#
#  service_account {
#    email  = google_service_account.default.email
#    scopes = ["cloud-platform"]
#  }
#
#  boot_disk {
#    initialize_params {
#      image = module.gce-container.source_image
#    }
#  }
#
#  metadata = {
#    gce-container-declaration = module.gce-container.metadata_value
#  }
#
#  labels = {
#    container-vm = module.gce-container.vm_container_label
#  }
#
#  network_interface {
#    network = "default"
#    access_config {
#    }
#  }
#}
#
#resource "local_file" "internal_ip" {
#  content  = google_compute_instance.db_vm.network_interface[0].network_ip
#  filename = "../db_internal_ip.txt"
#}
#
#output "db_internal_ip" {
#  description = "The internal IP address of the deployed DB instance"
#  value       = google_compute_instance.db_vm.network_interface.0.network_ip
#}
