# academy-devops-iac

This projects was created for learning purposes.
Objective: Create two instances on GCP with Terraform and configure them with Ansible.

# To make it work: 
- Create a project on GCP;
- Install Terraform;
- Install Ansible;
- Configure local SSH keys 
Ps: Ansible is agent-less, it applies all the changes via SSH and for that to happen, we need our public keys configured at our GCP instances and also inform our private key when running our playbook.

# What Terraform is doing on [app.tf](https://gitlab.com/roseane.silva/academy-devops-iac/-/blob/main/terraform/app.tf):

- [Configure Terraform to save its state into GCP Cloud Storage](https://gitlab.com/roseane.silva/academy-devops-iac/-/blob/main/terraform/app.tf#L2). This way we can apply and destroy resources without relying in a state file created locally.
- [Creates a Service Account](https://gitlab.com/roseane.silva/academy-devops-iac/-/blob/main/terraform/app.tf#L16);
- [Applies a role called roles/compute.admin](https://gitlab.com/roseane.silva/academy-devops-iac/-/blob/main/terraform/app.tf#L21) - which enables access to all Compute Engine features - to the Service Account we just created;
- [Creates a VM instance for the App](https://gitlab.com/roseane.silva/academy-devops-iac/-/blob/main/terraform/app.tf#L29), based on Debian and the default network;
- [Create a firewall rule](https://gitlab.com/roseane.silva/academy-devops-iac/-/blob/main/terraform/app.tf#L56) to allow incoming TCP traffic from any IPv4 source;
- [Saves the App external IP to a file](https://gitlab.com/roseane.silva/academy-devops-iac/-/blob/main/terraform/app.tf#L74) so we can use this IP later with Ansible.

# What Terraform is doing on [database-docker.tf](https://gitlab.com/roseane.silva/academy-devops-iac/-/blob/main/terraform/database-docker.tf):

- [Create a gce-container module](https://gitlab.com/roseane.silva/academy-devops-iac/-/blob/main/terraform/database-docker.tf#L1) so we can run a Docker container at our instance;
- [Creates a VM instance for the DB](https://gitlab.com/roseane.silva/academy-devops-iac/-/blob/main/terraform/database-docker.tf#L10), based on the Container-Optimized OS (COS) image, which is an operating system image for the Compute Engine VM that is optimized for running Docker containers.

# What Ansible is doing on [playbook.yaml](https://gitlab.com/roseane.silva/academy-devops-iac/-/blob/main/ansible/playbook.yaml)
All the changes will be applied to the host informed on [hosts](https://gitlab.com/roseane.silva/academy-devops-iac/-/blob/main/ansible/hosts) file. For that case, I only need configuration changes on the App instance, that's why we only have one host there.

- [Install Java SDK](https://gitlab.com/roseane.silva/academy-devops-iac/-/blob/main/ansible/playbook.yaml#L9): Installs the JDK in the App instance (my-cloud-app) via apt-get (as we have an instance based on Debian);
- [Create gradle directory](https://gitlab.com/roseane.silva/academy-devops-iac/-/blob/main/ansible/playbook.yaml#L15): Creates a directory where we'll have Gradle installed;
- [Install unzip](https://gitlab.com/roseane.silva/academy-devops-iac/-/blob/main/ansible/playbook.yaml#L20): Install unzip so we can unarchive Gradle which will be downloaded yet;
- [Unarchive Gradle file that needs to be downloaded](https://gitlab.com/roseane.silva/academy-devops-iac/-/blob/main/ansible/playbook.yaml#L25): Downloads and unarchive Gradle 7.1.1, adding it to the directory we just created "/opt/gradle";
- [Add Gradle Home variable](https://gitlab.com/roseane.silva/academy-devops-iac/-/blob/main/ansible/playbook.yaml#L31): Next steps create a gradle.sh file under profile.d directory, add Gradle to our PATH and make that gradle.sh file readable, writable and executable;
- [Create projects directory](https://gitlab.com/roseane.silva/academy-devops-iac/-/blob/main/ansible/playbook.yaml#L47): Create a directory to place our source code;
- [Install Git](https://gitlab.com/roseane.silva/academy-devops-iac/-/blob/main/ansible/playbook.yaml#L52): Install git, so we can clone the App repository;
- [Clone Git project](https://gitlab.com/roseane.silva/academy-devops-iac/-/blob/main/ansible/playbook.yaml#L57): Clone the repository;
- [Edit App host](https://gitlab.com/roseane.silva/academy-devops-iac/-/blob/main/ansible/playbook.yaml#L60): Edits the mongo db host at application.properties file, so the App can access the DB instance (This IP is the internal IP for the DB host)
- [Build App](https://gitlab.com/roseane.silva/academy-devops-iac/-/blob/main/ansible/playbook.yaml#L66): Build the App generating the jar file under build/libs directory, so when we access the instance we just need to Run the App

# What Ansible is doing on [playbook-artifactory.yaml](https://gitlab.com/roseane.silva/academy-devops-iac/-/blob/main/ansible/playbook-artifatory.yaml)
- [Download App jar from Artifactory](https://gitlab.com/roseane.silva/academy-devops-iac/-/blob/main/ansible/playbook_artifactory.yaml#L51) and place it on the GCP instance. This way we don't have to build the App inside the instance.
